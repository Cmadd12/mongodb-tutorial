# This markdown file will contain steps for installing and configuring MongoDB

1. Please visit the official MongoDB website at this [location](https://www.mongodb.com).

2. Next please enter your email and "Get Started Free".

3. Now it is time to select your cluster. Please select the cloud provider that aligns with your interests. This tutorial will have utilized a cluster provided by Amazon Web Services. Please note if cost is a concern make sure to select options with only "Free teir" marked.

<img src="pictures/2.png" alt="drawing" width="200"/>

4. Next name your cluster! Select the "create cluster" button.

5. Now it is time to configure your cluster to ensure not just anyone can enter! Please select the security tab from the browser page.

6. Now select the add new user. Please fill in your user name and password.
    - *IMPORTANT* Please remember your password as you will need it to log into your cluster later in the tutorial.

7. Select "Atlas Admin" as the user privilege.

<img src="pictures/Screen_Shot_2019-02-08_at_4.25.15_PM.png" alt="drawing" width="200"/>

8. Now it is time to also configure the whitelists of the IP address that can access your cluster. For easy configuration I recommend that you use "add current IP address".

<img src="pictures/Screen_Shot_2019-02-08_at_4.25.45_PM.png" alt="drawing" width="200"/>

9. Now access your cluster from the Overview screen. Select the "connect" button. We will first connect through the terminal. Please note this is tutorial is installed via mac.

<img src="pictures/Screen_Shot_2019-02-08_at_4.32.14_PM.png" alt="drawing" width="200"/>

10. Select the applicable operating system and download the shell. This shell will allow you to interact with MongoDB via command line. Keep this window open while you install the shell.

11. Now open your terminal and navigate to where you downloaded the MongoDB Shell. 
```
$cd mongodb-osx-x86_64-4.0.6/
$cd bin
```
12. Now copy the mongo shell command from the MongoDB site
```
$./mongo "mongodb+srv://<cluster_name>mongodb.net/test" --username <username> 
$Password
$show dbs
```
13. You should see something like the following:
    - admin  0.000GB
    - local  0.085GB

### *Congratulations you have successfull installed MongoDB!*

