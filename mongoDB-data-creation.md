# This markdown file will contain steps for generating data in your first mongoDB database/collection
1. Create a new database with the name 'vehicle-database'.
```
$use vehicles-database 
```

2. Now perform show dbs. Do you see your database?
```
$show dbs 
```
3. In order to see your database in the show dbs command you must have a collection assigned to it. Let us add a collection as well as some data.
```
$db.createCollection("vehicles");

$db.vehicles.insert({type: 'Semi', make: 'Mack'})
```
4. Let see what the data looks like.
```
$db.vehicles.find() 
```
5. Now lets add more data to the vehicles database. Do we have to have all fields filled to insert data?
```
$db.vehicles.insertMany([{type: 'Semi'}, {type: 'Semi', make: 'Volvo'}])
```
6. As you can see the insert occurred. Lets perform a db.collections.find() to understand the data structure.
```
$db.vehicles.find() 

MongoDB Enterprise CollinsCluster-shard-0:PRIMARY> db.vehicles.find()
{ "_id" : ObjectId("5c69db6c91f87c0fa041de66"), "type" : "Semi", "make" : "Mack" }
{ "_id" : ObjectId("5c69dc8491f87c0fa041de67"), "type" : "Semi" }
{ "_id" : ObjectId("5c69dc8491f87c0fa041de68"), "type" : "Semi", "make" : "Volvo" }
```
7. Now lets add a larger amount of data. Note how working in the terminal can get a little taxing when your import size grows.
``` 
db.vehicles.insertMany([{type: 'Semi', make: 'Ford'}, {type: 'Semi', make: 'Kenworth'}, {type: 'Semi', make: 'Mack'}, {type: 'Semi', make: 'Mack', color: 'Red'}, {type: 'Semi', make: 'Mack'}, {type: 'Semi', make: 'Ford', color: 'Red'}, {type: 'Semi', make: 'Kenworth'}, {type: 'Semi', make: 'Mack', color: 'White'}, {type: 'Semi', make: 'Ford'}, {type: 'Semi', make: 'Mack', color: 'Black'}])
```
8. How about I show you an easier way to import similar amounts of data by simply referencing a mongo java script(.mjs) file. Please download this file for the following exercise.  [Here](https://gitlab.com/Cmadd12/mongodb-tutorial/blob/master/Step%202:%20Data%20Creation%20File/more_semi.js)
    - Save the file on your desktop under a folder called "untitled folder".
    - Once complete run the following command in your terminal with the appropriate working directory structure. (i.e. maddco is specific to me!)
``` 
load("/Users/maddco/desktop/untitled folder/more_semi.mjs")
```
9. Lets see how the data sctructure has changed.
``` 
db.vehicles.find()
true

{ "_id" : ObjectId("5c69db6c91f87c0fa041de66"), "type" : "Semi", "make" : "Mack" }
{ "_id" : ObjectId("5c69dc8491f87c0fa041de67"), "type" : "Semi" }
{ "_id" : ObjectId("5c69dc8491f87c0fa041de68"), "type" : "Semi", "make" : "Volvo" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de69"), "type" : "Semi", "make" : "Ford" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de6a"), "type" : "Semi", "make" : "Kenworth" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de6b"), "type" : "Semi", "make" : "Mack" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de6c"), "type" : "Semi", "make" : "Mack", "color" : "Red" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de6d"), "type" : "Semi", "make" : "Mack" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de6e"), "type" : "Semi", "make" : "Ford", "color" : "Red" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de6f"), "type" : "Semi", "make" : "Kenworth" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de70"), "type" : "Semi", "make" : "Mack", "color" : "White" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de71"), "type" : "Semi", "make" : "Ford" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de72"), "type" : "Semi", "make" : "Mack", "color" : "Black" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de73"), "type" : "Semi", "make" : "Ford", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de74"), "type" : "Semi", "make" : "Kenworth", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de75"), "type" : "Semi", "make" : "Mack", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de76"), "type" : "Semi", "make" : "Mack", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de77"), "type" : "Semi", "make" : "Mack", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de78"), "type" : "Semi", "make" : "Ford", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de79"), "type" : "Semi", "make" : "Kenworth", "color" : "Green" }
```
10. Now that we have imported all of our data lets start to query it. How about narrowing the data down to Semis that are the color of green.
```
db.vehicles.find({"color": 'Green'})
```
11. What if we wanted to narrow down even further to semis that are green and the make of Macks. Is this the outcome you expected? I didnt =) there is more than just the color green now.
```
db.vehicles.find({make: 'Mack'}, {"color": 'Green'})

{ "_id" : ObjectId("5c69db6c91f87c0fa041de66") }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de6b") }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de6c"), "color" : "Red" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de6d") }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de70"), "color" : "White" }
{ "_id" : ObjectId("5c69e1a791f87c0fa041de72"), "color" : "Black" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de75"), "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de76"), "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de77"), "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de7a"), "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de7c"), "color" : "Green" }
```
12. So lets work on adding an "and" operation. Perform the following querey and see if it is what you originall expected?! Notice how we can also see the make and type as well.

```
db.vehicles.find({ $and:[{make: 'Mack'}, {"color": 'Green'}]})

{ "_id" : ObjectId("5c69e3c191f87c0fa041de75"), "type" : "Semi", "make" : "Mack", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de76"), "type" : "Semi", "make" : "Mack", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de77"), "type" : "Semi", "make" : "Mack", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de7a"), "type" : "Semi", "make" : "Mack", "color" : "Green" }
{ "_id" : ObjectId("5c69e3c191f87c0fa041de7c"), "type" : "Semi", "make" : "Mack", "color" : "Green" }
```
13. After learning some simple filtering operation lets use this to make changes to the data. You are told that in every instance of the color "Red" that this was emntered incorrectly. This needs to be changed to "Alizarin Crimson".
```
db.vehicles.update(
   { color: 'Red' },
   { $set:
      {
        color: 'Alizarin Crimson',
      }
   }
)
```
14. One of the final topics will be removing a document in the vehicles collection. There was an anamoly when the data was created and one make documents had a value error of "Volvo". Let us correct that now.
```
try {
   db.vehicles.deleteOne( { "_id" : ObjectId("5c69dc8491f87c0fa041de68") } );
} catch (e) {
   print(e);
}

{ "acknowledged" : true, "deletedCount" : 1 }
```
### *Congratulations you have successfull survived the introduction of importing, transforming and removing data in MongoDB!*
